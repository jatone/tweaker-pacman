package pacman

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"bitbucket.org/jatone/tweaker"
)

// PKGBuild name of the build file.
const PKGBuild = "PKGBUILD"

// makepkg name of the command.
const makepkg = "makepkg"

// MakepkgOptions see man makepkg
// https://www.archlinux.org/pacman/makepkg.conf.5.html
type MakepkgOptions struct {
	Directory         string
	BuildPackage      string
	Force             bool
	Log               bool
	PackageDirectory  string
	LoggingDirectory  string
	BuildDirectory    string
	SourceDirectory   string
	SourceDestination string
}

// CommandArgs returns an array of command line arguments based on the flags
func (t MakepkgOptions) CommandArgs() []string {
	args := []string{}

	if t.Log {
		args = append(args, "-L")
	}

	if t.Force {
		args = append(args, "-f")
	}

	return args
}

// EnvironmentVariables returns an array of environment variables that have
// be configured.
func (t MakepkgOptions) EnvironmentVariables() []string {
	environs := []string{}
	exists := func(s string) bool {
		return len(strings.TrimSpace(s)) > 0
	}

	if exists(t.LoggingDirectory) {
		environs = append(environs, fmt.Sprintf("LOGDEST=%s", t.LoggingDirectory))
	}

	if exists(t.BuildDirectory) {
		environs = append(environs, fmt.Sprintf("BUILDDIR=%s", t.BuildDirectory))
	}

	if exists(t.PackageDirectory) {
		environs = append(environs, fmt.Sprintf("PKGDEST=%s", t.PackageDirectory))
	}

	if exists(t.SourceDestination) {
		environs = append(environs, fmt.Sprintf("SRCDEST=%s", t.SourceDestination))
	}

	if exists(t.SourceDirectory) {
		environs = append(environs, fmt.Sprintf("SRCPKGDEST=%s", t.SourceDirectory))
	}

	return environs
}

// Makepkg executes the makepkg command with the provided options.
func Makepkg(opts MakepkgOptions) error {
	cmd := exec.Command(makepkg, opts.CommandArgs()...)
	cmd.Env = append(os.Environ(), opts.EnvironmentVariables()...)
	cmd.Dir = opts.Directory
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// MakepkgTask creates a tweaker.Task that executes makepkg in configured directory
func MakepkgTask(opts MakepkgOptions) tweaker.Task {
	return tweaker.TaskFunc(func() error {
		return Makepkg(opts)
	})
}
