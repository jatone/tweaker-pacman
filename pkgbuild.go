package pacman

import (
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"hash"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

type PkgBuildDetails struct {
	Name    string
	Version string
	// Release for the specified package version.
	Release     uint
	Description string
	URL         string
	Arch        []string
	License     []string
	Sources     []string
	MD5         []string
	SHA1        []string
	Options     []string
	Provides    []string
	Install     string
	Depends     struct {
		Required []string
		Optional []string
		Conflict []string
		Make     []string
	}
}

func (t *PkgBuildDetails) Context() (r RawPackageDetails) {
	return r.extract(*t)
}

func (t *PkgBuildDetails) Apply(visitors ...PkgBuildDetailsVistor) {
	for _, visitor := range visitors {
		visitor.Visit(t)
	}
}

type PkgBuildDetailsVistor interface {
	Visit(*PkgBuildDetails)
}

type SourceFactory interface {
	NewSource() Source
}

// Sets the source information in the PkgBuildDetails
type Source interface {
	io.Writer
	PkgBuildDetailsVistor
}

func NewMD5Source() Source {
	return checksumVisitor{
		Hash:        md5.New(),
		setChecksum: md5Checksum,
	}
}

func NewSHA1Source() Source {
	return checksumVisitor{
		Hash:        sha1.New(),
		setChecksum: sha1Checksum,
	}
}

type checksumVisitor struct {
	hash.Hash
	setChecksum func(pbd *PkgBuildDetails, checksum string)
}

func (t checksumVisitor) Visit(pbd *PkgBuildDetails) {
	t.setChecksum(pbd, hex.EncodeToString(t.Hash.Sum(nil)))
}

func sha1Checksum(pbd *PkgBuildDetails, checksum string) {
	pbd.SHA1 = append(pbd.SHA1, checksum)
}

func md5Checksum(pbd *PkgBuildDetails, checksum string) {
	pbd.MD5 = append(pbd.MD5, checksum)
}

func MultiChecksum(sourceFuncs ...func() Source) Source {
	writers := make([]io.Writer, 0, len(sourceFuncs))
	sources := make([]Source, 0, len(sourceFuncs))

	for _, sourceFunc := range sourceFuncs {
		source := sourceFunc()
		writers = append(writers, io.Writer(source))
		sources = append(sources, source)
	}

	return multiSource{
		Writer:  io.MultiWriter(writers...),
		sources: sources,
	}
}

type multiSource struct {
	io.Writer
	sources []Source
}

func (t multiSource) Visit(pbd *PkgBuildDetails) {
	for _, source := range t.sources {
		source.Visit(pbd)
	}
}

type fileSource struct {
	checksums PkgBuildDetailsVistor
	info      os.FileInfo
	path      string
}

func (t fileSource) Visit(pbd *PkgBuildDetails) {
	pbd.Sources = append(pbd.Sources, t.info.Name())
	t.checksums.Visit(pbd)
}

type SourcesFactory struct {
	MakepkgOptions  *MakepkgOptions
	PkgBuildDetails *PkgBuildDetails
	SourceBuilders  []func() Source
}

func (t SourcesFactory) isPKGBUILD(info os.FileInfo) bool {
	return t.MakepkgOptions.BuildPackage == info.Name()
}

func (t SourcesFactory) isInstall(info os.FileInfo) bool {
	return t.PkgBuildDetails.Install == info.Name() || "INSTALL" == info.Name()
}

func (t SourcesFactory) isPackage(info os.FileInfo) bool {
	return strings.HasSuffix(info.Name(), ".pkg.tar.xz")
}

func (t SourcesFactory) PopulateSources(dir string) error {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return err
	}

	for _, info := range files {
		var f *os.File
		path := filepath.Join(dir, info.Name())
		if err != nil {
			return err
		}

		// Skip directories, PKGBUILD, install script, or pkg.
		if info.IsDir() || t.isPKGBUILD(info) || t.isInstall(info) || t.isPackage(info) {
			continue
		}

		if f, err = os.Open(path); err != nil {
			return err
		}

		checksums := MultiChecksum(t.SourceBuilders...)
		copied, err := io.Copy(checksums, f)
		if err != nil {
			return err
		}

		if copied != info.Size() {
			return fmt.Errorf("failed to hash entire file %s - %d/%d %t", info.Name(), copied, info.Size(), t.isPackage(info))
		}

		visitor := fileSource{
			checksums: checksums,
			info:      info,
			path:      path,
		}

		visitor.Visit(t.PkgBuildDetails)
		return nil
	}

	return err
}

// String quoting helper functions
func singleQuote(items []string) string {
	return arrayQuoteHelper(items, `'`)
}

func doubleQuote(items []string) string {
	return arrayQuoteHelper(items, `"`)
}

func arrayQuoteHelper(items []string, quote string) string {
	if len(items) == 0 {
		return ""
	}

	return fmt.Sprintf("%s%s%s", quote, strings.Join(items, quote+" "+quote), quote)
}

type RawPackageDetails struct {
	Name    string
	Version string
	// Release for the specified package version.
	Release     uint
	Description string
	URL         string
	Arch        string
	License     string
	Sources     string
	MD5         string
	SHA1        string
	Options     string
	Provides    string
	Install     string
	Depends     struct {
		Required string
		Optional string
		Conflict string
		Make     string
	}
}

func (t RawPackageDetails) extract(d PkgBuildDetails) RawPackageDetails {
	t.Name = d.Name
	t.Version = d.Version
	t.Release = d.Release
	t.Description = d.Description
	t.URL = d.URL
	t.Arch = singleQuote(d.Arch)
	t.License = singleQuote(d.License)
	t.Sources = doubleQuote(d.Sources)
	t.MD5 = singleQuote(d.MD5)
	t.SHA1 = singleQuote(d.SHA1)
	t.Options = strings.Join(d.Options, " ")
	t.Install = d.Install
	t.Depends.Required = singleQuote(d.Depends.Required)
	t.Depends.Make = singleQuote(d.Depends.Make)
	t.Depends.Conflict = singleQuote(d.Depends.Conflict)
	t.Depends.Optional = singleQuote(d.Depends.Optional)

	return t
}
