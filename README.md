# Pacman Plugin for Tweaker.

plugin for the tweaker framework for generating pacman packages. Currently under development. Not stable.

## How do I get set up? ##
### Dependencies
```
#!bash
go get bitbucket.org/jatone/tweaker
```

## Contribution guidelines ##
Currently under heavy development once it stabilizes tests will be created/required for contributions.