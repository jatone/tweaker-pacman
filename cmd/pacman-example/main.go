package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/jatone/tweaker"
	"bitbucket.org/jatone/tweaker-pacman"
	"bitbucket.org/jatone/tweaker/downloads"
	"bitbucket.org/jatone/tweaker/templating"
)

var logger = log.New(os.Stderr, "pacman-example ", log.LstdFlags)

func die(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	workingDirectory, err := os.Getwd()
	die(err)

	pipe := make(chan tweaker.ExecutableTask)
	for i := 0; i < 5; i++ {
		go tweaker.Executer(i, pipe)
	}

	configuration := tweaker.Configuration{
		WorkingDirectory: tweaker.NewPather(workingDirectory),
		CompilerFactory:  tweaker.NewCompilerFactory(pipe),
	}

	definition1.Execute(configuration)
}

var definition1 = tweaker.DefinitionFunc(func(configuration tweaker.Configuration) {
	cFactory := configuration.CompilerFactory.Name("pacman-example").Logger(logger)
	compiler := cFactory.Logger(logger).Compiler()

	packageDirectory := configuration.WorkingDirectory.Pather("packages", "pacman")
	loggingDirectory := configuration.WorkingDirectory.Pather("logs", "pacman")

	buildDirectory := configuration.WorkingDirectory.Pather("build", "pacman-example")
	resourceDirectory := configuration.WorkingDirectory.Pather("resources", "pacman")

	err := tweaker.MultiMkdir(
		0755,
		packageDirectory.BaseDir(), loggingDirectory.BaseDir(),
		buildDirectory.BaseDir(), resourceDirectory.BaseDir(),
	)

	if err != nil {
		logger.Println("failed creating directories, bailing out", err)
		return
	}

	pbd := pacman.PkgBuildDetails{
		Name:        "HelloWorld",
		Version:     "1.5.1",
		Release:     0,
		Description: "Hello World",
		Install:     "HelloWorld",
		URL:         "https://golang.org",
		Arch:        []string{"x86_64"},
		Options:     []string{"!strip"},
	}

	opts := pacman.MakepkgOptions{
		BuildPackage:      pacman.PKGBuild,
		SourceDestination: buildDirectory.BaseDir(),
		SourceDirectory:   buildDirectory.BaseDir(),
		Directory:         buildDirectory.BaseDir(),
		PackageDirectory:  packageDirectory.BaseDir(),
		LoggingDirectory:  loggingDirectory.BaseDir(),
		Force:             true,
		Log:               true,
	}

	sources := pacman.SourcesFactory{
		SourceBuilders:  []func() pacman.Source{pacman.NewMD5Source},
		PkgBuildDetails: &pbd,
		MakepkgOptions:  &opts,
	}

	archive := fmt.Sprintf("go%s.%s-%s.tar.gz", "1.5.1", "linux", "amd64")
	dlf := downloader.HTTP.
		URL(fmt.Sprintf("https://storage.googleapis.com/golang/%s", archive)).
		DownloadTo(buildDirectory.Path(archive), 0755)

	cpt := tweaker.Copy(
		resourceDirectory.Path("INSTALL.template"),
		buildDirectory.Path(pbd.Install),
		0755,
	)
	tasks := downloader.DownloadTasks(dlf)

	compiler.Compile(append(tasks, cpt)...)
	compiler.Compile(tweaker.TaskFunc(func() error {
		return sources.PopulateSources(buildDirectory.Path())
	}))

	pkgbuild := templating.MaybeCache(
		templating.NewCache(resourceDirectory.Path("*.template")),
	).Template("PKGBUILD.template").
		WithCtx(pbd.Context()).
		TransformToFile(buildDirectory.Path(opts.BuildPackage), 0755)
	compiler.Compile(templating.TemplateTasks(pkgbuild)...)
	compiler.Compile(pacman.MakepkgTask(opts))
})
